import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BufferIntf extends Remote {
    public void put (int o) throws RemoteException;
}