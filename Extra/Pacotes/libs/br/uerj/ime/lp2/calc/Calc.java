package br.uerj.ime.lp2.calc;
import br.uerj.ime.lp2.exc.*;

public interface Calc {
	public int soma (int a, int b) throws ErrParEx;
	public int sub (int a, int b) throws ErrParEx;
	public double mult (double a, double b);
	public double div (double a, double b) throws Div0Ex;
}
