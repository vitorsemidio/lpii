public class Logica {
    private int numeroPreposicoes;
    // private boolean[][] array;

    public Logica( int numeroPreposicoes ) {
        this.numeroPreposicoes = numeroPreposicoes;
    }


    public boolean E(boolean a, boolean b) {
        return a == b;
    }
    public boolean OU(boolean a, boolean b) {
        return a || b;
    }
    public boolean IMPL(boolean a, boolean b) {
        return !a || b;
    }
    public boolean SSS(boolean a, boolean b) {
        return a != b;
    }
    public boolean XOU(boolean a, boolean b) {
        return (a || b) && (a != b);
    }
}