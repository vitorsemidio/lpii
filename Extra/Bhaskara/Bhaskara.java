import java.util.Scanner;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Bhaskara {
    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in);
        System.out.println("Informe a b c para executar a formula de Bhaskara");
        for (int i = 0; i < 10; i++) {
            int a = scan.nextInt(); int b = scan.nextInt(); int c = scan.nextInt();
            imprimirBhaskara(a, b, c);
        }
    }

    private static void imprimirBhaskara(int a, int b, int c) {
        double delta = calcularDelta(a, b, c);
        double r1; double r2;
        System.out.println(formatarEquacao(a, b, c));
        if (delta < 0) {
            System.out.println("Delta Negativo. Logo, as raizes sao imaginarias");
        } else if (delta == 0) {
            r1 = (-b + sqrt(delta))/(2*a);
            r2 = r1;
            System.out.format("Delta igual a 0. Logo, as raizes sao iguais: %.2f%n", r1);
        } else {
            r1 = (-b - sqrt(delta))/(2*a);
            r2 = (-b + sqrt(delta))/(2*a);
            System.out.format("Primeira raiz: %.2f   ", r1);
            System.out.format("Segunda raiz: %.2f%n", r2);
        }
    }

    private static double calcularDelta(int a, int b, int c) {
        return pow(b, 2) - (4 * a * c);
    }

    private static String formatarEquacao(int a, int b, int c) {
        return String.format("%+dx² %+dx %+d = 0", a, b, c);
    }
}