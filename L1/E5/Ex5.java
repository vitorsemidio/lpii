import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;
import static java.lang.Double.parseDouble;;

public class Ex5 {
    public static void main(String[] args) {
        BufferedReader stream = new BufferedReader( new InputStreamReader ( System.in ) );
        int quantidade;
        try {
            System.out.println("Digite o numero de pessoas: ");
            quantidade = parseInt(stream.readLine());
            ArrayList<PessoaIMC> pessoas = new ArrayList<>(quantidade);
            
            for (int i = 0; i < quantidade; i++) {
                System.out.println(i+1);
                PessoaIMC pessoa = readData(stream);   
                pessoas.add(pessoa);
                
            }
            for (PessoaIMC pessoa : pessoas) {
                System.out.println("---------");
                System.out.println(pessoa);
                System.out.println("---------");
            }
        } catch( IOException e ) {

        } catch( NumberFormatException e ) {

        } catch( Exception e) {
            
        }
        
        
    }

    public static PessoaIMC readData(BufferedReader stream) throws IOException {
        char sexo;
        String nome;
        String dataNascimento;
        double peso;
        double altura;

        sexo = readSexo(stream);        
        System.out.println("Digite o nome:");
        nome = stream.readLine();
        System.out.println("Digite a data de nascimento:");
        dataNascimento = stream.readLine();
        peso = readPeso(stream);
        altura = readAltura(stream);

        return sexo == 'm' ? 
            new Mulher(nome, dataNascimento, peso, altura) : 
            new Homem(nome, dataNascimento, peso, altura);
    }

    public static char readSexo(BufferedReader buffer) throws IOException {
        System.out.println("Inserir homem (h) ou mulher (m): ");
        char sexo = buffer.readLine().toLowerCase().trim().charAt(0);
        if (sexo != 'm' && sexo != 'h') {
            System.out.println("Sexo invalido");
            return readSexo(buffer);
        }
        return sexo;
    }
    public static double readPeso(BufferedReader buffer) throws IOException {
        System.out.println("Digite o peso:");
        double peso = parseDouble(buffer.readLine());
        if (peso < 0) {
            System.out.println("Peso Negativo Invalido");
            return readPeso(buffer);
        }
        return peso;
    }
    public static double readAltura(BufferedReader buffer) throws IOException {
        System.out.println("Digite a altura (em metros)");
        try {
            double altura = parseDouble(buffer.readLine());
            if (altura < 0) {
                System.out.println("Altura Negativa Invalida");
                return readAltura(buffer);
            }
            return altura;
        } catch (NumberFormatException e) {
            System.out.println("A altura deve ser um numero real");
            return readAltura(buffer);
        }
    }

}