import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Integer.parseInt;
import static java.lang.Double.parseDouble;

public class Ex3 {
    private static String[] unidades = {"primeiro", "segundo", "terceiro", "quarto", "quinto", "sexto", "setimo", "oitavo", "nono"};
    private static String[] dezenas = {"decimo", "vigesimo", "trigesimo", "quadragesimo", "quinquagesimo", "sexagesimo", "septuagesimo", "octogesimo", "nonagesimo"};
    private static int indexAngulo = 0;
    public static void main( String[] args ) {
        double degree;
        int qtdAngulos;
        AnguloObj[] anguloObj;

        qtdAngulos = lerQuantidadeAngulos();
        anguloObj = new AnguloObj[qtdAngulos];
        for (int i = 0; i < qtdAngulos; i++) {
            degree = lerAngulo();
            anguloObj[i] = new AnguloObj(degree);
        }

        showResult( anguloObj );

    }

    private static void showResult( AnguloObj[] anguloObj ) {
        System.out.println("Resultado ============");
        for (AnguloObj ang : anguloObj) {
            System.out.println(ang.toString());
        }
    }


    public static String RuleBasedNumberFormat( int numero ) {
        if ( numero <= 0 ) {
            return "";
        } else if ( numero < 10 ) {
            return unidades[numero-1];
        } else if ( numero < 100 ) {
            return numero == 10 ? 
                dezenas[ numero/10 - 1 ] : 
                dezenas[ numero/10 - 1 ] + " " + RuleBasedNumberFormat( numero % 10 );
        } else {
            return "";
        }
    }

    private static int lerQuantidadeAngulos() {
        BufferedReader inData;
        inData = new BufferedReader( new InputStreamReader ( System.in ) );
        System.out.println("Digite a quantidade de angulos: ");
        int qtdAngulos = 0;
        try {
            qtdAngulos = parseInt(inData.readLine());
            return qtdAngulos;
        } catch ( IOException e ) {
            System.out.println("Sem leitura do teclado");
        } catch ( NumberFormatException e ) {
            System.out.println("Formato de numero invalido");
        }
        return qtdAngulos;        
    }


    private static double lerAngulo() {
        BufferedReader inData;
        double angulo = 0;
        inData = new BufferedReader( new InputStreamReader ( System.in ) );
        System.out.format("Digite a medida em graus do %s angulo%n", RuleBasedNumberFormat( ++indexAngulo ));
        
        try {
            angulo = parseDouble( inData.readLine() );
        } catch ( IOException e ) {
            System.out.println("Sem leitura do teclado");            
        } catch ( NumberFormatException e ) {
            System.out.println("Formato de numero invalido");
        }

        return angulo;
    }
}