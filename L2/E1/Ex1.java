import java.io.*;
import java.util.*;
public class Ex1{
	public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
	public static int opcao;
	public static MinhaListaOrdenavel lista;
	public static void main(String args[]){
		lista = new MinhaListaOrdenavel(10);
		
		lista.getArray().add(new PessoaIMC("Eren","13/02/1987",65,1.62));
		lista.getArray().add(new PessoaIMC("Levi","15/11/1980",78,1.75));
		lista.getArray().add(new PessoaIMC("Mikasa","01/01/2001",55,1.73));
		lista.getArray().add(new PessoaIMC("Grisha","05/08/1986",95,1.77));
		lista.getArray().add(new PessoaIMC("Zeke","27/06/1990",48,1.66));
		lista.getArray().add(new PessoaIMC("Armin","25/01/1990",125,1.86));
		lista.getArray().add(new PessoaIMC("Annie","28/07/1985",82,1.69));
		lista.getArray().add(new PessoaIMC("Historia","27/02/1997",54,1.78));
		lista.getArray().add(new PessoaIMC("Erwin","27/04/1979",59,1.58));
		lista.getArray().add(new PessoaIMC("Ymir","23/03/1980",79,1.81));
		
		
		try{
			while(true){
				System.out.println("\nEscolha uma opcao:\n1.Imprimir Lista\n2.Sair");
				opcao = Integer.parseInt(buffer.readLine());
				if (opcao == 1) {
					ordenarLista();
				} else if (opcao == 2) {
					break;
				} else {
					System.out.println("\nOpcao Invalida!");
					continue;
				}
			}			
		} catch(NumberFormatException | IOException e){
			System.out.println("\nEntrada Invalida!");
		}
	}
	public static void ordenarLista(){
		
		while(true){
			try{
				System.out.println("\nSelecione o metodo de ordenacao:\n1. A-Z\n2. Z-A\n3. Maior Altura\n4. Menor Altura\n5. Maior Peso\n6. Menor Peso\n7. Maior IMC\n8. Menor IMC");
				opcao = Integer.parseInt(buffer.readLine());
				if(opcao>=1 && opcao<=8){
					ArrayList arrayOrdenado = lista.ordena(opcao);
					print(arrayOrdenado);
				}
				else{
					System.out.println("\nOpcao Invalida!");
					continue;
				}
				break;
			}
			catch(NumberFormatException | IOException e){
				System.out.println("\nEntrada Invalida!");
			}
		}
	}
	
	public static void print(ArrayList array){
		for(int i=0;i<array.size();i++)
			System.out.println(array.get(i)+"\n");
	}
}