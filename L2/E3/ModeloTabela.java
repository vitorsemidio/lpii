import javax.swing.table.*;
import javax.swing.event.*;
import java.util.*;

public class ModeloTabela extends AbstractTableModel{
	private int linhas = 20;
	private Object nomeColunas[];
	private Object dadosTabela[][];
	public ModeloTabela(Object dados[][],Object[] nC){
		dadosTabela = dados;
		nomeColunas = nC;
	}
	public void addTableModelListener(TableModelListener L){
		
	}
	
	public Class<Object> getColumnClass(int columnIndex){
		return Object.class;
	}
	public int getColumnCount(){
		return 5;
	}
	public String getColumnName(int columnIndex){
		return (String)nomeColunas[columnIndex];
	}
	public int getRowCount(){
		return linhas;
	}
	public Object getValueAt(int row, int column){
		return dadosTabela[row][column];
	}
	
	public boolean isCellEditable(int row, int column){
		return false;
	}
	public void removeTableModelListener(TableModelListener L){
			
	}
	public void setValueAt(Object value, int row, int column){
		dadosTabela[row][column] = value;
		fireTableCellUpdated(row, column);  
          
	}
	
	public void addPessoa(PessoaIMC p){      
        dadosTabela[dadosTabela.length] = p.toString().split(" ");  
		int ultimoIndice = getRowCount()-1;  
		fireTableRowsInserted(ultimoIndice, ultimoIndice);  
    }  
}