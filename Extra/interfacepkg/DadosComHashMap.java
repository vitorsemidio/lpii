import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author Guilherme Bechtinger
 */
public class DadosComHashMap implements DadosInterface{
    HashMap<Integer,Integer> dados;
    public DadosComHashMap(){
          dados = new HashMap();
    }

    public int getValor(int posicao) {
        return dados.get(posicao);
    }

    public void setValor(int posicao, int valor) {
        dados.put(posicao, valor);
    }
    @Override
    public String toString(){
        String retorno = "DADOS EM HASH MAP, IMPRIMINDO:\n";
        Object[] keys = dados.keySet().toArray();
        for(int x=0;x<keys.length;x++) retorno+="POSICAO " + x + " : " + getValor((Integer)keys[x]) + "\n";
        return retorno;
    }
}
