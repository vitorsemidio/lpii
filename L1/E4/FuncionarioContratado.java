public class FuncionarioContratado extends Funcionario {
    private int dependentes;
    private double salarioFamilia;
    private final double valorPorDep = 9.58;
    private final double aliquotaIR = 0.15;

    public FuncionarioContratado( String nome, String codEmpregado, double salario, int dependentes) {
        super(nome, codEmpregado, salario);
        this.dependentes = dependentes;
        calculaSalario( dependentes );
    }


    public void calculaSalario() {
        calculaSalario( this.aliquotaIR );
    }

    public void calculaSalario( int numeroDependentes ) {
        this.salarioFamilia = this.valorPorDep * numeroDependentes;
        super.acrescentarSalarioFamilia(this.salarioFamilia);
        this.calculaSalario();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(super.toString());
        sb.append(String.format("Salario-Base: %.2f %n", super.getSalarioBase()));
        sb.append(String.format("Salario-Liquido: %.2f %n", super.getSalarioLiquido()));
        sb.append("--------\n");
        return new String(sb);
    }
}