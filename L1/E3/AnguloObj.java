import static java.lang.Math.toRadians;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.tan;

public class AnguloObj {
    private double arcoRad;


    AnguloObj ( double angulo ) {
        this.arcoRad = angulo;
        this.arcoRad = converteAngulo();
    }

    public double converteAngulo ( ) {
        return toRadians( this.arcoRad );
    }

    public double funcaoSeno ( ) {
        return sin( this.arcoRad );
    }

    public double funcaoCosseno ( ) {
        return cos( this.arcoRad );
    }

    public double funcaoTangente ( ) {
        return tan( this.arcoRad );
    }

    public double funcaoCotangente () {
        return 1.0 / tan( this.arcoRad );
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Arco: %.2f%n", this.arcoRad));
        sb.append(String.format("Seno: %.2f%n", funcaoSeno()));
        sb.append(String.format("Cosseno: %.2f%n", funcaoCosseno()));
        sb.append(String.format("Tangente: %.2f%n", funcaoTangente()));
        sb.append(String.format("Cotangente: %.2f%n", funcaoCotangente()));
        return new String(sb);
    }
}