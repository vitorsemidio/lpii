import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    ArrayList<Block> blockchain = new ArrayList<>();

    public static void main(String[] args) {

        String[] genesisTransactions = {"Satoshi sent Emidio 10 bitcoins", "Emidio received 10 bitcoins from Satoshi"};
        Block genesisBlock = new Block( 0, genesisTransactions );

        String[] block2Transactions = {"oi", "hello"};
        Block block2 = new Block(genesisBlock.getBlockHash(), block2Transactions);

        System.out.println(genesisBlock.getBlockHash());
        System.out.println(block2.getBlockHash());
    }
}