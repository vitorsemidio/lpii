public class Pessoa{
	private String nome;
	private String dataNascimento;
	
	public Pessoa(String n, String dN){
		nome = n;
		dataNascimento = dN;
	}
	public String toString(){
		return nome+" "+dataNascimento;
	}
	public String getNome(){
		return nome;
	}
	public String getaNasc(){
		return dataNascimento;
	}
}