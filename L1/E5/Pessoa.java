public class Pessoa {
    String nome;
    String dataNascimento;

    public Pessoa(String _nome, String _dataNascimento) {
        this.nome = _nome;
        this.dataNascimento = _dataNascimento;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append( String.format("Nome: %s%nData de Nascimento: %s%n", this.nome, this.dataNascimento) );
        
        return new String(sb);
    }
}