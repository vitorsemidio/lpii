import java.util.*;
public class MinhaListaOrdenavel{
	private ArrayList<PessoaIMC> array;
	
	public MinhaListaOrdenavel(int capacity){
		array = new ArrayList<PessoaIMC>(capacity);
	}
	public ArrayList getArray(){return array;}
	
	public Comparator Nome = new Comparator(){
		public int compare(Object n1, Object n2){
			PessoaIMC pessoa1 = (PessoaIMC)n1; 
			PessoaIMC pessoa2 = (PessoaIMC)n2;
			String s1 = pessoa1.getNome();
			String s2 = pessoa2.getNome();
			return s1.compareTo(s2);
		}
	};
	public Comparator Altura = new Comparator(){
		public int compare(Object a1, Object a2){
			PessoaIMC pessoa1 = (PessoaIMC)a1; 
			PessoaIMC pessoa2 = (PessoaIMC)a2;
			double s1 = pessoa1.getAltura();
			double s2 = pessoa2.getAltura();
			if(s2>s1) return 1;
			else if(s2==s1) return 0;
			return -1;
		}
	};
	public Comparator Peso = new Comparator(){
		public int compare(Object p1, Object p2){
			PessoaIMC pessoa1 = (PessoaIMC)p1; 
			PessoaIMC pessoa2 = (PessoaIMC)p2;
			double s1 = pessoa1.getPeso();
			double s2 = pessoa2.getPeso();
			if(s2>s1) return 1;
			else if(s2==s1) return 0;
			return -1;
		}
	};
	public Comparator IMC = new Comparator(){
		public int compare(Object p1, Object p2){
			PessoaIMC pessoa1 = (PessoaIMC)p1; 
			PessoaIMC pessoa2 = (PessoaIMC)p2;
			double s1 = pessoa1.calculaIMC(pessoa1.getPeso(),pessoa1.getAltura());
			double s2 = pessoa2.calculaIMC(pessoa2.getPeso(),pessoa2.getAltura());
			if(s2>s1) return 1;
			else if(s2==s1) return 0;
			return -1;
		}
	};

	
	public ArrayList ordena(int criterio){
		switch(criterio){
			case 1:
				Collections.sort(array, Nome);
				break;
			case 2:
				Collections.sort(array, Nome.reversed());
				break;
			case 3:
				Collections.sort(array, Altura);
				break;
			case 4:
				Collections.sort(array, Altura.reversed());
				break;
			case 5:
				Collections.sort(array, Peso);
				break;
			case 6:
				Collections.sort(array, Peso.reversed());
				break;
			case 7:
				Collections.sort(array, IMC);
				break;
			case 8:
				Collections.sort(array, IMC.reversed());
				break;
		}
		return array;
	}
	
}