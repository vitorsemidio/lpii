public class Mulher extends PessoaIMC {

    public Mulher(String nome, String dataNascimento, double peso, double altura) {
        super(nome, dataNascimento, peso, altura);
    }

    public String resultIMC() {
        double imc = calculaIMC(this.altura, this.peso);
        
        if ( imc < 19) {
            return String.format("IMC: %.2f Abaixo do Peso", imc);
        } else if ( imc <= 25.8) {
            return String.format("IMC: %.2f Peso Ideal", imc);
        } else {
            return String.format("IMC: %.2f Acima do Peso Ideal", imc);
        }
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(super.toString());
        sb.append(resultIMC());
        return new String(sb);
    }
}