package lp2g39.biblioteca;
import java.util.*;
import java.io.*;

public class Biblioteca{
	private Hashtable <Integer,Usuario> usuarios;
	private Hashtable <String,Livro> livros;
	
	
	public Biblioteca(int capacity1, int capacity2){ 		
		usuarios = new Hashtable <Integer,Usuario>(capacity1);
		livros = new Hashtable <String,Livro>(capacity2);
		
	}
	
	public Biblioteca(String arquivoUsuario, String arquivoLivro){ 		
		leArquivo(usuarios, arquivoUsuario);
		leArquivo(livros, arquivoLivro);
		if(usuarios!=null && livros!=null)
		System.out.println("\nBiblioteca Carregada com sucesso!");
	}		
	
	public Hashtable<Integer,Usuario> getUsuarios(){return usuarios;}   
	public Hashtable<String,Livro> getLivros(){return livros;}			
	
	public void cadastraUsuario(Usuario user){usuarios.put(user.getCodigo(),user);}
	
	public void cadastraLivro(Livro book){livros.put(book.getCodigo(),book);}
	
	public void salvaArquivo(Hashtable tabela, String arquivo){			
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(arquivo));					
			out.writeObject(tabela);
			out.close();
			System.out.println("\nDados Salvos com sucesso!");
		}
		catch(IOException e){
			System.err.println("\n\tNao foi possivel gravar objeto no arquivo.");
		}
	}
	
	public void leArquivo(Hashtable tabela, String arquivo){			
		try{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(arquivo));
			if(tabela == usuarios)
				usuarios = (Hashtable<Integer,Usuario>)in.readObject();
			else if(tabela == livros)
				livros = (Hashtable<String,Livro>)in.readObject();
			in.close();
		}
		catch(FileNotFoundException e1){
			System.err.println("\n\tArquivo nao encontrado!");
			usuarios = null; livros = null;
		}
		catch(IOException | ClassNotFoundException e2){ 
			System.err.println("\n\tErro de leitura do arquivo!");
			usuarios = null; livros = null;
		}
	}
	
	public void emprestaLivro(Usuario us, Livro liv){		
		try{
			liv.empresta();
			liv.adUsuarioHist(us.getCodigo());
			us.adLivroHist(liv.getCodigo());
			
			System.out.println("\nLivro Emprestado!");
		}
		catch(CopiaNaoDisponivelEx e){
			System.err.println("\n\tNao ha mais copias disponiveis desse livro!");
		}
	}
	
	public void devolveLivro(Usuario us, Livro liv){
		try{
			liv.devolve();
			
			int indexL = liv.getIndexEmprestadoPara(us.getCodigo());
			liv.getHistorico().get(indexL).devolucao();
			
			int indexU = us.getIndexEmprestimo(liv.getCodigo());
			us.getHistorico().get(indexU).devolucao();
			System.out.println("\nLivro Devolvido!");
		}	
		catch(NenhumaCopiaEmprestadaEx e){
			System.err.println("\\tNao ha copias emprestadas desse livro!");
		}
		
	}
	
	public Comparator tituloLivro = new Comparator(){  
		public int compare(Object o1, Object o2){
			Livro L1 = (Livro)o1; 
			Livro L2 = (Livro)o2;
			String s1 = L1.getTitulo();
			String s2 = L2.getTitulo();
			return s1.compareTo(s2);
		}
	};
	
	public String imprimeLivros(){  
		String acervo = new String();
		ArrayList <Livro> array = new ArrayList <Livro> (livros.values());		
		
		Collections.sort(array, tituloLivro);
		for(int i=0 ; i< array.size() ; i++){
			Livro l = (Livro)array.get(i);
			acervo +="\n"+l.toString();
		}
		return acervo;
	}
	
	public Comparator nomeUsuario = new Comparator(){		
		public int compare(Object o1, Object o2){
			Usuario U1 = (Usuario)o1; 
			Usuario U2 = (Usuario)o2;
			String s1 = U1.getNome();
			String s2 = U2.getNome();
			return s1.compareTo(s2);
		}
	};
	
	public String imprimeUsuarios(){		
		String acervo = new String();
		ArrayList <Usuario> array = new ArrayList <Usuario> (usuarios.values());
		
		Collections.sort(array, nomeUsuario);
		for(int i=0 ; i< array.size() ; i++){
			Usuario U = (Usuario)array.get(i);
			acervo +="\n"+U.toString();
		}
		return acervo;
	}
	
	public Livro getLivro(String cod) throws LivroNaoCadastradoEx, NullPointerException{
		Livro liv = livros.get(cod);
		if(liv==null) throw new LivroNaoCadastradoEx();
			
		return liv;
	}
	
	public Usuario getUsuario(int cod) throws UsuarioNaoCadastradoEx, NullPointerException{	
			Usuario usu = usuarios.get(cod);
			if(usu==null) throw new UsuarioNaoCadastradoEx();
			
			return usu;
	}
	
	
	
}