/**
 *
 * @author Guilherme Bechtinger
 */
public class InterTeste3 {
    public void iniciar(){
       
 
 //      DadosInterface e = (DadosInterface) new DadosComVector();
//       executa (e);

         executa (new DadosComVector());

       System.out.println ("vec");

 //      DadosInterface d = (DadosInterface) new DadosComArray(10);
 //      executa (d);

         executa (new DadosComArray(10));

       System.out.println ("arr");


 //      DadosInterface f = (DadosInterface) new DadosComArquivo("banco.txt");
 //      this.executa ((DadosInterface)f);

         executa (new DadosComArquivo("banco.txt"));

       System.out.println ("bd");

 //      DadosInterface g = (DadosInterface) new DadosComHashMap();
 //      this.executa (g);       

         executa (new DadosComHashMap());

       System.out.println ("hm");


    }
    
    public void executa (DadosInterface dados) {

       dados.setValor(0, 45000);
       dados.setValor(1, 40000);
       dados.setValor(2, 300000);
       System.out.println(dados);
    }

    public static void main(String[] args) {
        InterTeste2 meusDados = new InterTeste2();
        meusDados.iniciar();
    }


}
