/**
 *
 * @author Guilherme Bechtinger
 */
public interface DadosInterface {

    public int getValor(int posicao);
    public void setValor(int posicao,int valor);
    public String toString();
    
}
