import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LetterCounter {
    public static boolean isALpha( char ch ) {
        // return Character.isLetter(ch);        
        return (ch >= 'A' && ch <= 'Z')
                || (ch >= 'a' && ch <= 'z');
    }

    public static int getIndex ( char c ) {
        int index = (int) Character.toUpperCase(c) - (int) 'A';
        return index;
    }

    public static void showResult ( int[] data ) {
        for ( int i = 0; i < data.length; i++ ) {
            System.out.format ( "A letra %s apareceu %d vezes\n", ( (char) (i + 'A') ), data[i] );
        }
    }

    public static void setFrequency( int[] source, String text ) {
        for ( int i = 0; i < text.length(); i++ ) {
            char ch = text.charAt(i);
            int index;
            if (isALpha(ch)) {
                index = getIndex(ch);
                source[index]++;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        int[] letterCount = new int[26];
        // ArrayList<Integer> listLetterCount = new ArrayList<Integer>(26);
        BufferedReader inData;
        inData = new BufferedReader( new InputStreamReader ( System.in ) );
        String line;
        do {
            System.out.print("Digite uma frase: ");
            line = inData.readLine();
            setFrequency(letterCount, line);
            showResult(letterCount);
        } while (line.length() != 0);

        System.out.println("\nFim");

        
    }

    
}