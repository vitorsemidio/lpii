import static java.lang.Math.toRadians;
import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.tan;

public class Angulo {
    public static double converteAngulo ( double graus ) {
        return toRadians( graus );
    }

    public static double funcaoSeno ( double angulo ) {
        return sin( angulo );
    }

    public static double funcaoCosseno ( double angulo ) {
        return cos( angulo );
    }

    public static double funcaoTangente ( double angulo ) {
        return tan( angulo );
    }

    public static double funcaoCotangente (double angulo) {
        return 1.0 / tan( angulo );
    }

    @Override
    public String toString() {
        return super.toString();
    }


}