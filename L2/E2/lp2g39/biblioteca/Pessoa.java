package lp2g39.biblioteca;
import java.util.*;
import java.io.*;

public class Pessoa implements Serializable{			
	private String nome;
	private GregorianCalendar dataNasc;
	
	public Pessoa(String n, int day, int month, int year){
		nome = n;
		dataNasc = new GregorianCalendar(year,month,day);
	}
	public String toString(){
		Date data = dataNasc.getGregorianChange();
		return "Nome: "+nome+"\tData de Nascimento: "+dataNasc.get(dataNasc.DATE)+"/"+dataNasc.get(dataNasc.MONTH)+"/"+dataNasc.get(dataNasc.YEAR);
	}
	public String getNome(){
		return nome;
	}
}