package lp2g39.biblioteca;
import java.util.*;

public class EmprestadoPara{
	private GregorianCalendar emp;
	private GregorianCalendar dev;
	int codigoUsuario;
	
	public EmprestadoPara(int cod){
		emp = new GregorianCalendar();
		codigoUsuario = cod;
	}
	
	public void devolucao(){
		dev = new GregorianCalendar();
	}
	
	public int getCodigoUsuario(){
		return codigoUsuario;
	}
	
	public String toString(){
		int mE = emp.get(emp.MONTH)+1; 
		String s = "Codigo do Usuario: "+codigoUsuario+"\tData do Emprestimo: "+
		emp.get(emp.DATE)+"/"+mE+"/"+emp.get(emp.YEAR)+"\tData de Devolucao: ";
		if(dev==null)
			return s+"Ainda Nao Devolvido";
		else{
			int mD = dev.get(dev.MONTH)+1;
			return s+dev.get(dev.DATE)+"/"+mD+"/"+dev.get(dev.YEAR);
		}
	}
	
}