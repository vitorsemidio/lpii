/**
 *
 * @author Guilherme Bechtinger
 */

public class DadosComArray implements DadosInterface{
    int[] dados;
    public DadosComArray(int tamanho){
         dados = new int[tamanho];
    }
    
    public int getValor(int posicao){
             return dados[posicao];
    }
    
   
    public void setValor(int posicao,int valor) {
             dados[posicao] = valor;
    }
    
    @Override
    public String toString(){
        String retorno = "DADOS EM ARRAY, IMPRIMINDO:\n";
        for(int x=0;x<dados.length;x++) retorno+="POSICAO " + x + " : " + getValor(x) + "\n";
        return retorno;
    }
}
