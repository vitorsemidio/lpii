import java.text.DecimalFormat;

public class PessoaIMC extends Pessoa{
	private double peso;
	private double altura;
	public PessoaIMC(String n, String dN, double p, double a){
		super(n,dN);
		peso = p;
		altura = a;
	}
	public double getPeso(){
		return peso;
	}
	public double getAltura(){
		return altura;
	}
	public double calculaIMC(double p, double a){
		return p/a/a;
	}
	public String toString(){
		DecimalFormat form = new DecimalFormat("0.00");
		return super.toString()+" "+peso+" "+altura+" "+form.format(calculaIMC(getPeso(),getAltura()));
	}
}