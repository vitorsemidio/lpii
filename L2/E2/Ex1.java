import lp2g39.biblioteca.*;
import java.io.*;
import java.util.*;

public class Ex1{
	private static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
	private static Biblioteca bib = new Biblioteca(500,500);
	public static int opcao;
	public static String erro = "\n\tEntrada Invalida!\n";
	public static String opcaoInvalida = "\n\tOpcao Invalida!\n";
	
	public static void main(String args[]){
		while(true){
			try{
				System.out.println("\n\tMENU PRINCIPAL\n\n1. Cadastro\n2. Emprestimo\n3. Carregar acervo de Usuarios e Livros\n4. Pesquisa\n5. Sair");
				opcao = Integer.parseInt(buffer.readLine());
				if(opcao==1){Cadastro();}
				else if(opcao==2){Emprestimo();break;}
				else if(opcao==3){carregarBiblioteca();System.out.println("\nBiblioteca carregada com sucesso!\n");}		
				else if(opcao==4){Relatorio();break;}		
				else if(opcao==5){break;}	
				else{System.err.println(opcaoInvalida);continue;}
			}
			catch(NumberFormatException|IOException e){System.err.println(erro);continue;}
		}
	}
	
	public static void carregarBiblioteca(){
		while(true){
			try{
				System.out.println("Insira o nome dos arquivos nos quais se encontram usuarios e livros, respectivamente:");
				String arq1 = buffer.readLine();
				String arq2 = buffer.readLine();
				bib = new Biblioteca(arq1,arq2);
				
			}
			catch(IOException e){System.err.println(erro); continue;}
			main(new String[1]);
		}
	}
	
	public static void Manutencao(Hashtable tabela){
		if(tabela == bib.getUsuarios()) bib.salvaArquivo(bib.getUsuarios(),"lp2g39/biblioteca/Usuarios.arq");
		else bib.salvaArquivo(bib.getLivros(),"lp2g39/biblioteca/Livros.arq");
	} 
	
	public static void Cadastro(){
		String menu = "\nCadastro:\nSelecione uma opcao:\n1. Cadastrar Usuario\n2. Cadastrar Livro\n3. Salvar em Arquivo\n4. Voltar";
		String instrucoesUsu = "Digite codigo e data de nascimento separados por espaco(inclusive o dia, mes e ano)";
		String instrucoesLiv = "Digite o codigo do livro e quantidade de copias separados por espaco";
		
		while(true){
			try{
				System.out.println(menu);
				opcao = Integer.parseInt(buffer.readLine());
				switch(opcao){
					case 1:
						System.out.println(instrucoesUsu);
						String vetor[] = buffer.readLine().split(" ");
						if(vetor.length!=4){System.err.println("Erro! Numero de argumentos inesperado!");continue;}
						System.out.print("Insira o nome:");
						String name = buffer.readLine();
						System.out.print("Insira o Endereco: ");
						String endereco = buffer.readLine();
						bib.cadastraUsuario(new Usuario(name, Integer.parseInt(vetor[1]), Integer.parseInt(vetor[2]), Integer.parseInt(vetor[3]), endereco, Integer.parseInt(vetor[0])));
						System.out.println("\nUsuario cadastrado com sucesso!");
						break;
					case 2:
						System.out.println(instrucoesLiv);
						String vetor2[] = buffer.readLine().split(" ");
						if(vetor2.length!=2){System.err.println("Erro! Numero de argumentos inesperado!");continue;}
						System.out.print("Insira o nome do livro: ");
						String nome = buffer.readLine();
						System.out.print("Insira a categoria do livro: ");
						String categoria = buffer.readLine();
						bib.cadastraLivro(new Livro(vetor2[0], nome, categoria, Integer.parseInt(vetor2[1]), 0));
						System.out.println("\nLivro cadastrado com sucesso!");
						break;
					case 3:
						System.out.println("1. Salvar Usuarios\n2. Salvar Livros");
						opcao = Integer.parseInt(buffer.readLine());
						switch(opcao){
							case 1:
								Manutencao(bib.getUsuarios());
								break;
							case 2:
								Manutencao(bib.getLivros());
								break;
							default:
								System.err.println(opcaoInvalida);
						}
						break;
					case 4: main(new String[1]); break;
					default: System.err.println(opcaoInvalida);
						
				}
			}
			catch(NumberFormatException|IOException e){System.out.println(erro);continue;}
		}
	}
	
	public static void Emprestimo(){
		
		while(true){
			try{
				System.out.println("\nEmprestimo:\n1. Exibir Cadastro de Livros\n2. Fazer Emprestimo\n3. Devolucao\n4. Voltar");
				opcao = Integer.parseInt(buffer.readLine());
				if(opcao==1){ bib.imprimeUsuarios();break;}
				else if(opcao==2 || opcao==3){
					System.out.print("Insira o codigo do usuario: ");
					Usuario U = bib.getUsuario(Integer.parseInt(buffer.readLine()));
					System.out.print("Insira o codigo do livro: ");
					Livro L = bib.getLivro(buffer.readLine());
					if(opcao==2) bib.emprestaLivro(U,L);
					else{bib.devolveLivro(U,L);}
				}
				else if(opcao==4){main(new String[1]);break;}
				else{System.err.println(opcaoInvalida);continue;}
			}
			catch(NumberFormatException|IOException e1){System.err.println(erro);continue;}
			catch(UsuarioNaoCadastradoEx e2){System.err.println("\n\tUsuario Nao Cadastrado!");continue;}
			catch(LivroNaoCadastradoEx e3){System.err.println("\n\tLivro Nao Cadastrado!");continue;}
		}
	}
	
	public static void Relatorio(){
		while(true){
			try{
				String menu = "\n1. Listar todos os Usuarios\n2. Listar acervo Livros\n3. Pesquisar Usuario\n4. Pesquisar Livro\n5. Voltar";
				System.out.println(menu);
				opcao = Integer.parseInt(buffer.readLine());
				switch(opcao){
					case 1: System.out.println(bib.imprimeUsuarios()); break;
					case 2: System.out.println(bib.imprimeLivros()); break;
					case 3:
						System.out.print("Insira o codigo do usuario: ");
						System.out.println("\n"+bib.getUsuario(Integer.parseInt(buffer.readLine())));
						break;
					case 4:
						System.out.print("Insira o codigo do livro: ");
						System.out.println("\n"+bib.getLivro(buffer.readLine()));
						break;
					case 5: main(new String[1]); break;
					default: System.err.println(opcaoInvalida); continue;
				}
			}
			catch(NumberFormatException|IOException e1){System.err.println(erro);continue;}
			catch(UsuarioNaoCadastradoEx e2){System.err.println("\n\tUsuario nao cadastrado!");continue;}
			catch(LivroNaoCadastradoEx|NullPointerException e3){System.err.println("\n\tLivro nao cadastrado!");continue;}
			
		}
	}

}
