public class Calculadora implements Calc {

	Calculadora() {

	}

	public int soma (int a, int b) throws ErrParEx {
		if (a > 100 || b > 100) {
			throw new ErrParEx();
		}
		return a + b;
	}

	public int sub (int a, int b) throws ErrParEx {
		if ( b > a ) {
			throw new ErrParEx();
		}
		return a - b;
	}

	public double mult (double a, double b) {
		return a * b;
	}

	public double div (double a, double b) throws Div0Ex {
		if ( b == 0) {
			throw new Div0Ex();
		}
		return a / b;
	}
}