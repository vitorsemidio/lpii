import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Scanner;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class Ex4 {

    private int numero;

    public Ex4(int numero) {
        this.numero = numero;
    }

    public static void main( String[] args ) {

        try {
            int numeroFuncionarios;
            Scanner scan = new Scanner ( System.in );  

            System.out.print("Informe o numero de funcionario: ");
            numeroFuncionarios = scan.nextInt();

            Ex4 ex4 = new Ex4(numeroFuncionarios);
            ex4.calculaSalarios();
            scan.close();
        } catch (InputMismatchException e ) {
            System.out.println("Erro: Numero de Funcionarios Invalido");
        } catch (NumberFormatException e) {
            System.out.println("Erro: Formato de Numero Invalido");
        } catch (IOException e) {
            System.out.println("Erro: Entrada de Dado Invalida");
        }

    }


    public void calculaSalarios() throws IOException, NumberFormatException {
        BufferedReader stream = new BufferedReader( new InputStreamReader ( System.in ) );
        Collection<FuncionarioContratado> colecao = new ArrayList<>();
        

        System.out.println("Cadastro de Funcionários");
        for (int i = 0; i < this.numero; i++) {
            String nome;
            String codEmpregado;
            double salario;
            int dependentes;

            System.out.print("Nome: ");
            nome = stream.readLine();

            System.out.print("Codigo: ");
            codEmpregado = stream.readLine();

            System.out.print("Salario: ");
            salario = parseDouble(stream.readLine());

            System.out.print("Numero de dependentes: ");
            dependentes = parseInt(stream.readLine());

            FuncionarioContratado funcionario = new FuncionarioContratado(nome, codEmpregado, salario, dependentes);
            colecao.add(funcionario);
            System.out.println();

        }



        System.out.println("\n--Folha Salarial--");
        for (FuncionarioContratado f : colecao) {
            System.out.println(f.toString());
        }

        
        
    }
}