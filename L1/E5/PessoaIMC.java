import static java.lang.Math.pow;

public abstract class PessoaIMC extends Pessoa{
    protected double peso;
    protected double altura;
    
    public PessoaIMC(String _nome, String _dataNascimento, double _peso, double _altura) {
        super(_nome, _dataNascimento);
        this.peso = _peso;
        this.altura = _altura;
    }

    public double getPeso() {
        return this.peso;
    }

    public double getAltura() {
        return this.altura;
    }

    public double calculaIMC( double altura, double peso ) {
        double imc = peso / pow(altura, 2);
        return imc;
    }

    public abstract String resultIMC();

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.toString());
        sb.append(String.format("Peso: %.2f%nAltura: %.2f%n", this.peso, this.altura));
        return new String(sb);
    }
}