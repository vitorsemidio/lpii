package lp2g39.biblioteca;
import java.util.*;
import java.io.*;

public class Usuario extends Pessoa{
	private String endereco;
	private int codigoUsuario;
	private Vector<Emprestimo> Historico; 
	
	public Usuario(String n, int d, int m, int y, String end, int cod){
		super(n,d,m,y);
		endereco = end;
		codigoUsuario = cod;
		Historico = new Vector<Emprestimo>(500,50); 
	}
	public void adLivroHist(String cod){
		Historico.add(new Emprestimo(cod));
	}
	
	public int getCodigo(){
		return codigoUsuario;
	}
	
	public int getIndexEmprestimo(String codigoLivro){
		for(int i=0; i<Historico.size();i++)
			if(Historico.get(i).getCodigoLivro() == codigoLivro)
				return i;
		return -1;
	}
	
	public Vector<Emprestimo> getHistorico(){
		return Historico;
	}
	public String getHistoricoToString(){
		String s = "";
		for(int i=0 ; i<Historico.size() ; i++)
			s += "\n\t"+Historico.get(i).toString();
		return s;
	} 
	
	public String toString(){
		return "Codigo: "+codigoUsuario+"\t"+super.toString()+"\tEndereco: "+endereco+"\n\tHistorico:"+getHistoricoToString()+"\n\n"; 
	}

}