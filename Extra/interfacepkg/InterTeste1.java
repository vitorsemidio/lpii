
/**
 *
 * @author Guilherme Bechtinger
 */
public class InterTeste1 {
    public void iniciar(DadosInterface dados){
       dados.setValor(0, 45000);
       dados.setValor(1, 40000);
       dados.setValor(2, 300000);
       System.out.println(dados);
    }
    
    public static void main(String[] args) {
        InterTeste1 meusDados = new InterTeste1();
   
        meusDados.iniciar(new DadosComArray(10));
        meusDados.iniciar(new DadosComVector());
        meusDados.iniciar(new DadosComArquivo("banco.txt"));
        meusDados.iniciar(new DadosComHashMap());
        
    }

}
