// import java.io.IOException;


public class Ex1 {
	public static void main(String[] args) {
		int tamanhoArgs = args.length;
		double[] dados = new double[tamanhoArgs];
		double resultado = 0.0;
		boolean success = true;
		switch (tamanhoArgs) {
			case 1:
				try {
					double raio = Double.parseDouble(args[0]);
					if (raio > 0) {
						resultado = calcula(raio);
						System.out.format("A area do circulo eh: %.2f\n", resultado);
					} else {
						mensagemLadoNegativo();
					}
				} catch(NumberFormatException e) {
					System.out.format ("%dº argumento, \"%s\", nao eh numero\n", (1), args[0]);
				}
				
				break;

			case 2:
				
				for (int i = 0; i < tamanhoArgs; i++) {
					try {
					} catch (NumberFormatException e) {
						dados[i] = Double.parseDouble(args[i]);
						System.out.format ("%dº argumento, \"%s\", nao eh numero\n", (i+1), args[i]);
						success = false;
					}
				}
				if (success) {
					if (dados[0] > 0 && dados[1] > 0) {
						resultado = calcula(dados[0], dados[1]);
						System.out.format("A area do retangulo eh: %.2f unidades de area.\n", resultado);
					} else {
						mensagemLadoNegativo();
					}
				}

				break;	

			case 3:
				for (int i = 0; i < tamanhoArgs; i++) {
					try {
						dados[i] = Double.parseDouble(args[i]);
					} catch (NumberFormatException e) {
						System.out.format ("%dº argumento, \"%s\", nao eh numero\n", (i+1), args[i]);
						success = false;
					}
				}

				if (success) {
					if (condicaoExistenciaTriangulo(dados[0], dados[1], dados[2])) {
						resultado = calcula(dados[0], dados[1], dados[2]);
						System.out.format("A area do triangulo eh: %.2f unidades de area.\n", resultado);
						System.out.format("O triangulo eh %s.", tipoTriangulo(dados[0], dados[1], dados[2]));
					} else {
						System.out.println("Valores informados nao formam um triangulo");
					}
				}
				
				break;

			default:
				if(tamanhoArgs == 0) {
					System.out.println("Numero de argumentos insuficiente");				
				} else {
					System.out.println("Numero de argumentos excessivo");
				}

		}
	}

	private static void mensagemLadoNegativo() {
		System.out.println("Uma ou mais medidas negativas");
	}

	private static double calcula(double r) {
		double area = Math.PI * Math.pow(r,2);
		return area;
	}

	private static double calcula(double b, double a) {
		double area = b * a;
		return area;
	}

	private static double calcula(double l1, double l2, double l3) {
		double p = (l1+l2+l3)/2;
		double area = Math.sqrt(Math.abs(p*(p-l1)*(p-l2)*(p-l3)));
		System.out.format("O triangulo eh %s\n", tipoTriangulo(l1, l2, l3));
		return area;
	}

	private static String tipoTriangulo(double l1, double l2, double l3) {

		if (l1 == l2 && l2 == l3) {
			return "Equilatero";
		} else if ( l1 != l2 && l1 != l3 && l2 != l3) {
			return "Escaleno";
		} else {
			return "Isosceles";
		}

	}

	private static boolean condicaoExistenciaTriangulo(double l1, double l2, double l3) {
		if (l1 <= 0 || l2 <= 0 || l3 <= 0) {
			return false;
		}

		if ( Math.abs(l2-l3) >= l1 && l1 <= (l2+l3) ) {
			return false;
		} else if ( Math.abs(l1-l3) >= l2 && l2 <= (l1+l3) ) {
			return false;
		} else if ( Math.abs(l1-l2) >= l3 && l2 <= (l1+l2) ) {
			return false;
		} 

		return true;
	}
}