import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class IMC extends JApplet implements ActionListener{
	public static Label Nome, DataNasc, Peso, Altura;
	public static JTextField nome, datanasc, peso, altura;
	public static Button Adicionar;
	public static Object nomeColunas[] = {"Nome","Data de Nascimento","Peso","Altura","IMC"};
	public static Object[][] dadosTabela =  new Object[20][5];
	public static ModeloTabela Modelo = new ModeloTabela(dadosTabela,nomeColunas);
	public static JTable tabela = new JTable(Modelo);
	public static int index = 0;
	public static JComboBox<Object> menuOrdenacao = new JComboBox<Object>(nomeColunas);
	
	
	public void init(){
		Nome = new Label("Insira o nome:",2);
		DataNasc = new Label("Insira a data de nascimento:",2);
		Peso = new Label("Insira o peso:",2);
		Altura = new Label("Insira o altura:",2);
		
		nome = new JTextField();
		datanasc = new JTextField();
		peso = new JTextField();
		altura = new JTextField();
		
		Adicionar = new Button("Adicionar");
		
		Nome.setSize(200,20); Nome.setLocation(101,30);
		nome.setSize(500,20); nome.setLocation(301,30);
		DataNasc.setSize(200,20); DataNasc.setLocation(101,55);
		datanasc.setSize(500,20); datanasc.setLocation(301,55);
		Peso.setSize(200,20); Peso.setLocation(101,80);
		peso.setSize(500,20); peso.setLocation(301,80);
		Altura.setSize(200,20); Altura.setLocation(101,105);
		altura.setSize(500,20); altura.setLocation(301,105);
		
		Adicionar.setSize(80,20); Adicionar.setLocation(460,130);
		Adicionar.addActionListener(this);
		
		
		tabela.setSize(900,300); tabela.setLocation(51,171);
		
		ActionListener nomeOrd = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
			}
		};
		ActionListener dataOrd = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
			}
		};
		ActionListener pesoOrd = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
			}
		};
		ActionListener alturaOrd = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
			}
		};
		ActionListener imcOrd = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				
			}
		};
		
		
		add(Nome); add(nome);
		add(DataNasc); add(datanasc); 
		add(Peso); add(peso); 
		add(Altura); add(altura);
		add(Adicionar);
		add(tabela);
		
		add(new Label(""));
		
		
	}
	
	public void actionPerformed(ActionEvent event){
	
		String n = nome.getText();
		String d = datanasc.getText();
		String p = peso.getText();
		String a = altura.getText();
		PessoaIMC pessoa = new PessoaIMC(n,d,Double.parseDouble(p),Double.parseDouble(a));
		dadosTabela[index++] = pessoa.toString().split(" ");
		
		Modelo = new ModeloTabela(dadosTabela,nomeColunas);
		tabela = new JTable(Modelo);
		tabela.setSize(900,300); tabela.setLocation(51,171);
		add(tabela);
		validate();
		
		
	}
}