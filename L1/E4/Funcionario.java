public class Funcionario {
    private String nome;
    private String codEmpregado;

    private double salario;
    private double salarioLiquido;
    private double salarioBase;

    public Funcionario( String nome, String codEmpregado, double salario ) {
        this.nome = nome;
        this.codEmpregado = codEmpregado;
        this.salario = salario;
        this.salarioBase = salario;
        this.salarioLiquido = salario;
    }

    public double calculaSalario ( double desconto ) {
        this.salarioLiquido = this.salarioBase * (1 - desconto);
        return this.salarioLiquido;
    }

    public void acrescentarSalarioFamilia(double salarioFamilia) {
        this.salarioBase += salarioFamilia;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Nome: %s %n", this.getNome()));
        sb.append(String.format("Codigo: %s %n", this.getCodEmpregado()));
        return new String(sb);
    }


    public String getNome() {
        return this.nome;
    }
    public String getCodEmpregado() {
        return this.codEmpregado;
    }
    public double getSalario() {
        return this.salario;
    }
    public double getSalarioLiquido() {
        return this.salarioLiquido;
    }
    public double getSalarioBase() {
        return this.salarioBase;
    }
}