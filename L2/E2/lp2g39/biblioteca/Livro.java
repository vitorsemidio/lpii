package lp2g39.biblioteca;
import java.util.*;
import java.io.*;

public class Livro implements Serializable{
	private String codigo, titulo, categoria;
	private int quantidade, emprestados;
	private Vector<EmprestadoPara> Historico;
	private int index;   
	
	public Livro(String cod, String ttl, String cat, int q, int empr){
		codigo = cod;
		titulo = ttl;
		categoria = cat;
		quantidade = q;
		emprestados = empr;
		Historico = new Vector<EmprestadoPara>(500,50);
	}
	public Livro(String ttl){
		titulo = ttl;
	}
	public void empresta()throws CopiaNaoDisponivelEx{
		if(emprestados+1> quantidade)
			throw new CopiaNaoDisponivelEx();
		emprestados++;
	}
	
	public int getIndexEmprestadoPara(int codigoUsuario){
		for(int i=0; i<Historico.size();i++)
			if(Historico.get(i).getCodigoUsuario() == codigoUsuario)
				return i;
		return -1;
	}
	
	public void devolve() throws NenhumaCopiaEmprestadaEx{
		if(emprestados==0)
			throw new NenhumaCopiaEmprestadaEx();
		emprestados--;
	}
	public void adUsuarioHist(int cod){
		Historico.add(new EmprestadoPara(cod));
	}
	public String getCodigo(){
		return codigo;
	}
	public String getTitulo(){
		return titulo;
	}
	public Vector<EmprestadoPara> getHistorico(){
		return Historico;
	}
	public String getHistoricoToString(){
		String s = "";
		for(int i=0 ; i<Historico.size() ; i++)
			s += "\n\t"+Historico.get(i).toString();
		return s;
	} 
	
	public String toString(){
		return "Codigo: "+codigo+"\tTitulo: "+titulo+"\tCategoria: "+categoria+"\tQuantidade: "+quantidade+
		"\tEmprestados: "+emprestados+"\n\tEmprestado para:"+getHistoricoToString()+"\n\n";
	}
}