import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Guilherme Bechtinger
 */
public class DadosComArquivo implements DadosInterface{
     BufferedReader leitura;
     ArrayList<Integer> dados;
     String arquivo;
     public DadosComArquivo(String arquivo){
        this.arquivo = arquivo;
        dados = new ArrayList();
        try {
            leitura = new BufferedReader(new FileReader(arquivo));
            while(leitura.ready()){
                  dados.add(new Integer(leitura.readLine()));
            }
        } catch (Exception ex) {
            System.out.println("Arquivo não encontrado.");
        }
     }

    public int getValor(int posicao) {
        return dados.get(posicao);
    }

    public void setValor(int posicao, int valor) {
        dados.set(posicao, valor);
        String temp ="";
        try {
             BufferedWriter gravar = new BufferedWriter(new FileWriter(arquivo));
             for(int x=0;x<dados.size();x++){
                 temp = "" + dados.get(x);
                 gravar.write(temp,0,temp.length());
                 gravar.newLine();
             }
             gravar.flush();
        } catch (IOException ex) {
            
        }
    }
     
    @Override
    public String toString(){
        String retorno = "DADOS EM ARQUIVO, IMPRIMINDO:\n";
        for(int x=0;x<dados.size();x++) retorno+="POSICAO " + x + " : " + getValor(x) + "\n";
        return retorno;
    }
     
}
