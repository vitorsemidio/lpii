import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Double.parseDouble;


public class Ex2 extends Angulo {
    public static void main(String[] args) {
        BufferedReader inData;
        inData = new BufferedReader( new InputStreamReader ( System.in ) );
        double degree;
        String line;

        if (args.length != 0) {
            for (String ang : args) {
                try {
                    degree = parseDouble(ang);
                    showResult(degree);                
                } catch ( NumberFormatException e) {
                    System.out.println("Formato de numero invalido");
                }
            }
        }

        try {
            do {
                System.out.println("Digite uma medida em graus do angulo:");
                line = inData.readLine();
                degree = parseDouble(line);
                showResult(degree);
    
            }   while ( !line.isEmpty() );
        } catch ( NumberFormatException e ) {

        } catch ( IOException e ) {
            
        }


        
    }

    private static void showResult( double d ) {
        double rad = Angulo.converteAngulo(d);

        System.out.format( "Seno: %.2f%n", Angulo.funcaoSeno( rad ) );
        System.out.format("Cosseno: %.2f%n", Angulo.funcaoCosseno( rad ) );
        System.out.format("Tangente: %.2f%n", Angulo.funcaoTangente( rad ) );
        System.out.format("Cotangente: %.2f%n", Angulo.funcaoCotangente( rad ) );
    }
}