import java.util.Vector;

/**
 *
 * @author Guilherme Bechtinger
 */
public class DadosComVector implements DadosInterface {
    Vector<Integer> dados;
    public DadosComVector(){
         dados = new Vector();
    }
    
    public int getValor(int posicao) {
            return dados.get(posicao);
    }
    
   
    public void setValor(int posicao,int valor) { 
            dados.add(posicao, valor);
    }
    
    @Override
    public String toString(){
        String retorno = "DADOS EM VECTOR, IMPRIMINDO:\n";
        for(int x=0;x<dados.size();x++) retorno+="POSICAO " + x + " : " + getValor(x) + "\n";
        return retorno;
    }
}
