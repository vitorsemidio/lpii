package lp2g39.biblioteca;
import java.util.*;

public class Emprestimo{
	private GregorianCalendar emp; 
	private GregorianCalendar dev;
	private String codigoLivro;
	
	public Emprestimo(String cod){
		emp = new GregorianCalendar();
		codigoLivro = cod;
	}
	
	public void devolucao(){
		dev = new GregorianCalendar();
	}
	
	public String getCodigoLivro(){
		return codigoLivro;
	}
	
	public String toString(){
		int mE = emp.get(emp.MONTH)+1; 
		String s = "Codigo do Livro: "+codigoLivro+"\tData do Emprestimo: "+emp.get(emp.DATE)
		+"/"+mE+"/"+emp.get(emp.YEAR)+"\tData de devolucao: ";
		if(dev==null)
			return s+"Ainda Nao Devolvido";
		else{
			int mD = dev.get(dev.MONTH)+1;
			return s+dev.get(dev.DATE)+"/"+mD+"/"+dev.get(dev.YEAR);
		}
	}
	
	
}